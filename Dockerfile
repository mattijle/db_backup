FROM node:stretch
RUN apt-get update
RUN apt-get install -y postgresql-client cron
ADD backup-cron /etc/cron.d/backup-cron
RUN mkdir /code
WORKDIR /code
ADD . /code/
RUN chmod 0644 /etc/cron.d/backup-cron
RUN chmod 0755 /code/start.sh
RUN chmod 0755 /code/backup.sh
RUN touch /var/log/cron.log
RUN npm install
