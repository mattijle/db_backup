const W = require('when');
const node = require('when/node');
const exec = node.lift(require('child_process').exec);
const moment = require('moment');
const fs = require('fs');
let DB = {}
DB['PORT'] = process.env.DB_PORT;
DB['HOST'] = process.env.DB_HOST;
DB['USER'] = process.env.DB_USER;
DB['PASS'] = process.env.DB_PASS;
DB['NAME'] = process.env.DB_NAME;
DB['DUMP_PREFIX'] = process.env.DB_DUMP_PREFIX;
const Year = moment().format('YYYY');
const Month = moment().format('MMMM');

const now = moment().format('DD-MM-YYYY_HH-mm-ss');1

const dump_name = DB['DUMP_PREFIX'] + "_" + now + '.sql';
let dir_name = '/backup/'+Year+'/'+Month;
let task = "PGPASSWORD=" + DB['PASS'] +" pg_dump -U " + DB['USER'] + " -h " + DB['HOST']  
					 + " -p " + DB['PORT']+ " " + DB['NAME'] + " > /code/" + dump_name; 
console.log("Starting with these options.");

for (var p in DB) {
	if(DB.hasOwnProperty(p)){
		console.log("DB_"+p+' = ' + DB[p]);
	}
}
let createBackUpDir = (year,month) => {
	if(!fs.existsSync('/backup/'+year))
		fs.mkdirSync('/backup/'+year);
	if(!fs.existsSync('/backup/'+year+'/'+month)){
		fs.mkdirSync('/backup/'+year+'/'+month);
	}
}
let onError = (err) => {
	console.log(err);
}

let runBackup  = () => {
	const dumpTask = exec(task, {maxBuffer:1024*1024}).then((r) => r[0],onError);
	W.all([dumpTask]).then(([dump]) => {
		fs.copyFile('/code/'+dump_name,dir_name + '/'+dump_name, (err)=>{
			if(err) throw err;
			console.log('Database dump completed to ' + dir_name + '/' + dump_name);
			fs.unlinkSync('/code/'+dump_name);
		});
	});
}
createBackUpDir(Year,Month);
runBackup();

