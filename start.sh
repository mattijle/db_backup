#!/bin/bash
echo "
	export DB_PORT=$DB_PORT
	export DB_HOST=$DB_HOST
	export DB_USER=$DB_USER
	export DB_PASS=$DB_PASS
	export DB_NAME=$DB_NAME
	export DB_DUMP_PREFIX=$DB_DUMP_PREFIX
	" > /env.sh

	service cron start
	tail -f /var/log/cron.log
